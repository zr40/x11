# Copyright 2022 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

# Update the gdk-pixbuf loader cache when installing or removing loaders

export_exlib_phases pkg_postinst pkg_postrm

GDK_PIXBUF_QUERY_LOADERS=/usr/$(exhost --target)/bin/gdk-pixbuf-query-loaders

update_gdk_pixbuf_cache() {
    illegal_in_global_scope

    [[ ${ROOT} != "/" ]] && return
    [[ ! -x ${GDK_PIXBUF_QUERY_LOADERS} ]] && return

    # This command is expected to fail on cross builds when host can't run target arch binaries.
    nonfatal edo ${GDK_PIXBUF_QUERY_LOADERS} --update-cache ||
        ewarn "Failed to update gdk-pixbuf loader cache"
}

gdk-pixbuf-loader_pkg_postinst() {
    illegal_in_global_scope

    update_gdk_pixbuf_cache
}

gdk-pixbuf-loader_pkg_postrm() {
    illegal_in_global_scope

    update_gdk_pixbuf_cache
}

