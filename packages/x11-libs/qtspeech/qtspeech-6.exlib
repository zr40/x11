# Copyright 2017, 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtSpeech"
DESCRIPTION="
The module enables a Qt application to support accessibility features such as
text-to-speech, which is useful for end-users who are visually challenged or
cannot access the application for whatever reason. The most common use case
where text-to-speech comes in handy is when the end-user is driving and cannot
attend the incoming messages on the phone. In such a scenario, the messaging
application can read out the incoming message."

MYOPTIONS="
    examples
    flite   [[ description = [ Build a plugin supporting Flite ] ]]
    speechd [[ description = [ Build a plugin supporting Speech Dispatcher ] ]]
"

QT_MIN_VER="$(ever range -4)"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        x11-libs/qtbase:${SLOT}[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:${SLOT}[>=${QT_MIN_VER}]
        flite? (
            app-speech/flite
            sys-sound/alsa-lib
            x11-libs/qtmultimedia:${SLOT}[>=${QT_MIN_VER}]
        )
        speechd? ( app-speech/speechd )
"

qtspeech-6_src_configure() {
    local cmake_params=(
        $(cmake_option examples QT_BUILD_EXAMPLES)

        $(qt_cmake_feature flite)
        $(qt_cmake_feature flite flite_alsa)
        $(qt_cmake_feature speechd)
    )

    ecmake "${cmake_params[@]}"
}

qtspeech-6_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtspeech-6_src_install() {
    cmake_src_install

    # Remove empty dir
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/qt6/libexec

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

