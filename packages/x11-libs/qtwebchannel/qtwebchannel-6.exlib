# Copyright 2014-2018, 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtWebChannel module"
DESCRIPTION="
It offers Qt applications a seamless way to publish QObjects for
interaction with HTML/JavaScript clients. These clients can either be inside
local Qt WebViews or any other, potentially remote, client which supports
JavaScript, as long as a communication channel such as WebSocket is available.
"

LICENCES+=" GPL-2"
MYOPTIONS="examples
    qml [[ description = [ Support for QtQuick and the QML language ] ]]
"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}]
        examples? ( x11-libs/qtwebsockets:${SLOT}[>=${PV}] )
        qml? ( x11-libs/qtdeclarative:${SLOT}[>=${PV}] )
"

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'examples QT_BUILD_EXAMPLES'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'examples Qt6WebSockets'
    'qml Qt6Quick'
)

qtwebchannel-6_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtwebchannel-6_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

