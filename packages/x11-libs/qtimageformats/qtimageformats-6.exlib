# Copyright 2013-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: Various image format plugins"
DESCRIPTION="Supports MNG, TGA, TIFF and WBMP".

LICENCES+=" GPL-2"
MYOPTIONS="jpeg2000 mng tiff
    webp [[ description = [ Build plugin to read and write Google's WebP image format ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}][gui]
        jpeg2000? ( media-libs/jasper[>=1.900.1] )
        mng? ( media-libs/libmng[>=1.0.9] )
        tiff? (
            media-libs/tiff:=[>=4.0]
            sys-libs/zlib
        )
        webp? ( media-libs/libwebp:= )
"

qtimageformats-6_src_configure() {
    local cmake_params=(
        $(qt_cmake_feature jpeg2000 jasper)
        $(qt_cmake_feature mng)
        $(qt_cmake_feature tiff)
        $(qt_cmake_feature webp)
    )

    ecmake "${cmake_params[@]}"
}

qtimageformats-6_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtimageformats-6_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

