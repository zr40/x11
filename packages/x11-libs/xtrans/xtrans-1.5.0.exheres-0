# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg [ suffix=tar.xz ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="X11 network protocol transport library"
DESCRIPTION="
xtrans is a library of code that is shared among various X packages to
handle network protocol transport in a modular fashion, allowing a
single place to add new transport types.  It is used by the X server,
libX11, libICE, the X font server, and related components.

It is however, *NOT* a shared library, but code which each consumer
includes and builds it's own copy of with various #ifdef flags to make
each copy slightly different.  To support this in the modular build
system, this package simply installs the C source files into
\$(prefix)/include/X11/Xtrans and installs a pkg-config file and an
autoconf m4 macro file with the flags needed to use it.
"

LICENCES="X11"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        x11-utils/util-macros
        doc? ( app-text/xmlto[>=0.0.22] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --without-fop )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'doc xmlto' )

src_prepare() {
    # fix sys-apps/xfs build error: sys/stropts.h: No such file or directory
    edo sed \
        -e 's:stropts.h:ioctl.h:g' \
        -i Xtranslcl.c

    # xtrans.pc contains arch dependent include
    edo sed -i -e '/pkgconfigdir/s/datadir/libdir/' Makefile.am

    autotools_src_prepare
}

