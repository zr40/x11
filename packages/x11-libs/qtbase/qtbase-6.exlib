# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'qt-5.exlib', which is:
#     Copyright 1999-2008 Gentoo Foundation
#     Copyright 2008-2010 Bo Ørsted Andresen <zlin@exherbo.org>
#     Copyright 2008-2009, 2010 Ingmar Vanhassel
#     Copyright 2013-2018 Heiko Becker <heirecka@exherbo.org>

require qt cmake [ ninja=true ]
require flag-o-matic

export_exlib_phases src_configure src_prepare src_compile src_install

SUMMARY="Qt Cross-platform application framework for desktop and embedded development"
DESCRIPTION="Contains the following modules:
- QtCore: non-graphical classes used by other modules
- QtConcurrent: Classes for writing multi-threaded programs w/o using low-level threading primitives
- QtDBus: Classes for inter-process communication over the D-Bus protocol
- QtGui: Base classes for graphical user interface (GUI) components
- QtNetwork: Classes to make network programming easier and more portable
- QtOpenGL: OpenGL support classes
- QtPrintSupport: Classes to make printing easier and more portable
- QtSql: Classes for database integration using SQL
- QtTest: Classes for unit testing Qt applications and libraries
- QtWidgets: Classes to extend Qt GUI with C++ widgets (belonged to QtGui in 4.x)
- QtXml: C++ implementations of SAX and DOM"

SQL_BACKENDS="mysql postgresql sqlite" # db2, interbase, oci, odbc

MYOPTIONS="cups examples debug glib gui journald kms opengles sql
    ( ${SQL_BACKENDS} ) [[ requires = sql ]]
    sql? (
        ( ${SQL_BACKENDS} ) [[ number-selected = at-least-one ]]
    )
    brotli      [[ description = [ Support for handling resources compressed with Brotli through QNetworkAccessManager ] ]]
    cups        [[ requires    = gui ]]
    examples    [[ requires    = gui ]]
    glib        [[ description = [ Add support for the glib eventloop ] ]]
    gtk         [[ description = [ Enable GTK+ style support, this will install a Qt4 style that renders using GTK+, to blend in with a GTK+ desktop ]
                   requires    = [ glib gui ] ]]
    gui         [[ description = [ Build GUI related modules ]
                   presumed    = true ]]
    journald    [[ description = [ Support sending logging output directly to systemd-journald ] ]]
    kerberos
    kms         [[ description = [ KMS platform plugin to run a single-window per screen without X11 ]
                   requires    = opengles ]]
    libinput    [[ description = [ Support input devices via libinput ]
                   requires = gui ]]
    libproxy    [[ description = [ Use libproxy for system proxy settings ] ]]
    opengles    [[ description = [ Use OpenGL ES2 rather than standard desktop OpenGL ]
                   requires    = gui ]]
    sql         [[ description = [ Build the QtSQL module ] ]]
    vulkan      [[ description = [ Support for rendering via the Vulkan graphics API ]
                   requires = gui ]]
    zstd        [[ description = [ Support for the zstd compression format with RCC ] ]]

    ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]

    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        app-text/xmlstarlet [[ note = [ minifies some mimetype xml files ] ]]
        virtual/pkg-config
        doc? (
            x11-libs/qttools:${SLOT}[>=6.5.1] [[
                note = [ 6.5.1 due to qtattributionscanner incompatibilities ]
            ]]
        )
        vulkan? ( sys-libs/vulkan-headers )
    build+run:
        app-admin/eclectic[>=2.0.18] [[ note = [ Split ld-*.path, @TARGET@ substitution ] ]]
        app-crypt/libb2
        dev-libs/double-conversion:=
        dev-libs/icu:=[>=50.1]
        dev-libs/pcre2[>=10.20]
        net-misc/publicsuffix-list
        sys-apps/dbus[>=1.2.0] [[ note = [ besides QtDBUs also needed for accessibility and xcb ] ]]
        sys-libs/zlib[>=1.0.8]
        brotli? ( app-arch/brotli )
        cups? ( net-print/cups[>=1.4] )
        glib? ( dev-libs/glib:2 )
        gtk? (
            dev-libs/atk
            x11-libs/gtk+:3[>=3.6][X]
            x11-libs/pango
        )
        gui? (
            app-text/md4c
            dev-libs/at-spi2-core[X?]
            media-libs/fontconfig
            media-libs/freetype:2[>=2.2]
            media-libs/libpng:=
            x11-dri/mesa
            x11-libs/harfbuzz[>=2.6.0]
            x11-libs/libICE
            x11-libs/libxkbcommon[>=0.5.0][X]
            x11-libs/libSM
            x11-libs/libX11
            x11-libs/libxcb[>=1.12]
            x11-libs/libXext
            x11-libs/libXrender[>=0.6]
            x11-libs/mtdev
            x11-utils/xcb-util-cursor[>=0.1.1]
            x11-utils/xcb-util-image[>=0.3.9]
            x11-utils/xcb-util-keysyms[>=0.3.9]
            x11-utils/xcb-util-renderutil[>=0.3.9]
            x11-utils/xcb-util-wm[>=0.3.9]
            providers:ijg-jpeg? ( media-libs/jpeg:= )
            providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        )
        journald? ( sys-apps/systemd ) [[ note = [ wanted for journald logging ] ]]
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        kms? ( x11-dri/libdrm )
        libinput? ( sys-libs/libinput )
        libproxy? ( net-libs/libproxy:1 )
        mysql? ( virtual/mysql )
        postgresql? ( dev-db/postgresql-client )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.1] )
        sqlite? ( dev-db/sqlite:3 )
        vulkan? ( sys-libs/vulkan-loader )
        zstd? ( app-arch/zstd[>=1.3] )
"

# Packages that need QtSQL should depend on x11-libs/qtbase:5[sql]
# option='sql' needs at least one SQL plugin, any-of mysql, postgresql, sqlite, (firebird, odbc)

# Tests need a running X server and network access (last checked: 5.1.1)
RESTRICT="test"

qtbase_mkspecs_dir() {
     # Allows us to define which mkspecs dir we want to use.
    local spec

    spec="linux"
    if [[ ${CXX} == *c++* ]]; then
        if cxx-is-gcc;then
            spec+="-g++"
        elif cxx-is-clang; then
            spec+="-clang"
            #if option providers:libc++; then
            #    spec+="-libc++"
            #fi
        else
            die "Unknown compiler ${CXX}; you will need to add a check for it to qt.exlib"
        fi
    else
        die "Unknown compiler ${CXX}"
    fi

    echo "${spec}"
}

qtbase-6_src_prepare() {
    cmake_src_prepare

    # Unfortunately qmake is still supported for the lifetime of Qt6 and
    # PyQt6 actually uses it, so we still need to do some modifications.

    # It's safe to use -gcc and -g++ here instead of -cc -and -cxx here,
    # because we only modifiy gcc mkspecs files.
    edo sed -e "s:QMAKE_CC.*=.*gcc:QMAKE_CC = $(exhost --target)-gcc:" \
            -e "s:QMAKE_CXX.*=.*g++:QMAKE_CXX = $(exhost --target)-g++:" \
            -i "${CMAKE_SOURCE}"/mkspecs/common/g++-base.conf

    # Fix the names of some unprefixed executables
    edo sed -e "s:\(PKG_CONFIG.*=.\)\(pkg-config\):\1${PKG_CONFIG}:" \
            -i "${CMAKE_SOURCE}"/mkspecs/features/qt_functions.prf
    edo sed -e "s:^\(QMAKE_AR.*=.\)\(ar.*\):\1$(exhost --tool-prefix)\2:" \
            -i "${CMAKE_SOURCE}"/mkspecs/common/linux.conf

    # Don't prestrip
    mkspecs="${CMAKE_SOURCE}"/mkspecs/$(qtbase_mkspecs_dir)/qmake.conf
    if ! grep -q CONFIG "${mkspecs}"; then
        # The linux-clang-libc++ qmake.conf does not set CONFIG, but includes another file.
        include=$(grep -Po "(?<=include\()[^)]+" ${mkspecs})
        mkspecs=$(dirname "${mkspecs}")/${include}
    fi

    edo sed \
        -e "/CONFIG/s:$: nostrip:" \
        -i "${mkspecs}"
}

qtbase-6_src_configure() {
    local host=$(exhost --target)

    local myconf=(
        -DINSTALL_BINDIR="/usr/${host}/lib/qt6/bin"
        -DINSTALL_INCLUDEDIR="/usr/${host}/include/qt6"
        -DINSTALL_PLUGINSDIR="/usr/${host}/lib/qt6/plugins"
        -DINSTALL_MKSPECSDIR="/usr/${host}/lib/qt6/mkspecs"
        -DINSTALL_ARCHDATADIR="/usr/${host}/lib/qt6"
        -DINSTALL_DATADIR="../share/qt6"
        -DINSTALL_DOCDIR="../share/doc/qt6"
        -DINSTALL_EXAMPLESDIR="../share/qt6/examples"
        -DINSTALL_SYSCONFDIR="/etc/xdg"

        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DBUILD_WITH_PCH:BOOL=TRUE

        # Qt modules
        -DFEATURE_concurrent:BOOL=ON
        -DFEATURE_dbus:BOOL=ON
        # TODO: Needs fixing with LibreSSL
        -DFEATURE_network:BOOL=ON
        # Avoids crashes of executable from other modules, e.g. assisstant or
        # designer if built with LTO: https://bugreports.qt.io/browse/QTBUG-112332
        # cf. https://code.qt.io/cgit/qt/qtbase.git/commit/?id=19b7f854a274812d9c95fc7aaf134a12530c105f
        -DFEATURE_no_direct_extern_access:BOOL=ON
        -DFEATURE_testlib:BOOL=ON
        -DFEATURE_xml:BOOL=ON

        -DFEATURE_dbus_linked:BOOL=ON
        -DFEATURE_directfb:BOOL=OFF
        # EGL Full Screen/Single Surface, probably only useful on embedded devices
        -DFEATURE_eglfs:BOOL=OFF
        -DFEATURE_icu:BOOL=ON
        # INTEGRITY Real-Time OS
        -DFEATURE_integrityhid:BOOL=OFF
        -DFEATURE_linuxfb:BOOL=OFF
        # Tracing
        -DFEATURE_ctf:BOOL=OFF
        # LTTng is an open source tracing framework for Linux
        -DFEATURE_lttng:BOOL=OFF
        # Use the system MIME database in $XDG_DATA_DIRS/mime instead of Qt's
        # bundled copy
        -DFEATURE_mimetype_database:BOOL=ON
        -DFEATURE_openssl_linked:BOOL=ON
        -DFEATURE_pcre2:BOOL=ON
        -DFEATURE_publicsuffix_system:BOOL=ON
        # Proprietary DBMS Mimer SQL
        -DFEATURE_sql_mimer:BOOL=FALSE
        -DFEATURE_syslog:BOOL=ON
        -DFEATURE_system_proxies:BOOL=ON
        -DFEATURE_system_zlib:BOOL=ON

        # Disable some SQL drivers
        -DFEATURE_sql_db2:BOOL=OFF      # IBM DB2 (version 7.1 and above)
        -DFEATURE_sql_ibase:BOOL=OFF    # Borland InterBase
        -DFEATURE_sql_oci:BOOL=OFF      # Oracle Call Interface Driver
        -DFEATURE_sql_odbc:BOOL=OFF     # Open Database Connectivity (ODBC) (e.g. MS SQL Server)

        # Touchscreen access library - unwritten
        -DFEATURE_tslib:BOOL=OFF

        # Qnx
        -DFEATURE_slog2:BOOL=OFF

        # Only for gui?
        -DINPUT_freetype="system"
        -DINPUT_libjpeg="system"
        -DINPUT_libmd4c="system"
        -DINPUT_libpng="system"

        -DQT_USE_CCACHE:BOOL=FALSE

        $(cmake_option examples QT_BUILD_EXAMPLES)
        $(qt_cmake_feature glib)

        # GUI related switches
        $(qt_cmake_feature gui)
        $(qt_cmake_feature gui accessibility)
        $(qt_cmake_feature gui egl)
        $(qt_cmake_feature gui evdev)
        $(qt_cmake_feature gui fontconfig)
        $(qt_cmake_feature gui gbm)
        $(qt_cmake_feature gui libudev)
        $(qt_cmake_feature gui mtdev)
        $(qt_cmake_feature gui opengl)
        $(qt_cmake_feature gui sessionmanager)
        $(qt_cmake_feature gui system_xcb_xinput)
        $(qt_cmake_feature gui widgets)
        $(qt_cmake_feature gui xcb)
        $(qt_cmake_feature gui xkbcommon)
        $(qt_cmake_feature gui xcb_xlib)
        $(qt_cmake_feature gui xcb_native_painting)

        $(qt_cmake_feature brotli)
        $(qt_cmake_feature cups)
        $(qt_cmake_feature gtk gtk3)
        $(qt_cmake_feature journald)
        $(qt_cmake_feature kerberos gssapi)
        $(qt_cmake_feature kms)
        $(qt_cmake_feature libinput)
        $(qt_cmake_feature libproxy)
        $(qt_cmake_feature mysql sql_mysql)
        $(qt_cmake_feature opengles opengles2)
        $(qt_cmake_feature postgresql sql_psql)
        $(qt_cmake_feature sql)
        $(qt_cmake_feature sqlite sql_sqlite)
        $(qt_cmake_feature vulkan)
        $(qt_cmake_feature zstd)
    )

    # TODO:
    # - BUILD_CMAKE_TESTING:BOOL=OFF (Build tests for the Qt build system)
    # - BUILD_TESTING:BOOL=OFF (Build the testing tree)

    ecmake "${myconf[@]}"
}

qtbase-6_src_compile() {
    cmake_src_compile

    if option doc ; then
        # html_docs target builds html docs, qch_docs QCH files (assistant), docs both
        eninja docs
    fi
}

qtbase-6_src_install() {
    cmake_src_install

    local host=$(exhost --target)

    hereenvd 46qt6 <<EOF
LDPATH=/usr/@TARGET@/lib/qt6
QT_PLUGIN_PATH=/usr/host/lib/qt6/plugins
COLON_SEPARATED="QT_PLUGIN_PATH"
EOF

    if option examples ; then
        # The examples might add some empty dirs. Since the examples are additional documentation,
        # there shouldn't be any empty dirs we might want to keep, so we remove them all in
        # one go.
        edo find "${IMAGE}"/usr/share/qt6/examples -type d -empty -delete
    fi

    # install documentation
    option doc && DESTDIR="${IMAGE}" eninja install_docs

    # remove references to build dir
    edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/${host}/lib/libQt6*.prl

    # Symlink qmake to qmake-qt6 in bin (Hopefully nobody relys on qmake
    # anymore one day and we can get rid of that...).
    edo mkdir "${IMAGE}"/usr/${host}/bin
    dosym /usr/${host}/lib/qt6/bin/qmake /usr/${host}/bin/qmake-qt6

    # TODO: Needs to be made co-installable with Qt5 in some way
    # disable debug output from applications by default
#    insinto /etc/xdg/QtProject
#    hereins qtlogging.ini <<EOF
#[Rules]
#*.debug=false
#EOF
}

