# Copyright 2012 Ivan Dives <ivan.dives@gmail.com>
# Copyright 2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=LibVNC tag=${PV} ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="A VNC server for real X displays"
DESCRIPTION="
x11vnc allows one to view remotely and interact with real X displays (i.e. a
display corresponding to a physical monitor, keyboard, and mouse) with any VNC
viewer. In this way it plays the role for Unix/X11 that WinVNC plays for Windows.

It has built-in SSL/TLS encryption and 2048 bit RSA authentication, including
VeNCrypt support; UNIX account and password login support; server-side scaling;
single port HTTPS/HTTP+VNC; Zeroconf service advertising; and TightVNC and
UltraVNC file-transfer. It has also been extended to work with non-X devices:
natively on Mac OS X Aqua/Quartz, webcams and TV tuner capture devices, and
embedded Linux systems such as Qtopia Core. Full IPv6 support is provided.

It also provides an encrypted Terminal Services mode (-create, -svc, or -xdmsvc
options) based on Unix usernames and Unix passwords where the user does not need
to memorise his VNC display/port number. Normally a virtual X session (Xvfb) is
created for each user, but it also works with X sessions on physical hardware.
"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    avahi

    ( libc: musl )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-proto/xorgproto
    build+run:
        net-libs/libvncserver[>=0.9.8]
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/libXrandr
        x11-libs/libXtst
        avahi? ( net-dns/avahi )
        !libc:musl? ( dev-libs/libxcrypt:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
    run:
        dev-lang/tk [[ note = [ wish command ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-colormultipointer
    --with-crypt
    --with-crypto
    --with-dpms
    --with-fbdev
    --with-fbpm
    --with-ssl
    --with-uinput
    --with-v4l
    --with-x
    --with-xcomposite
    --with-xdamage
    --with-xfixes
    --with-xinerama
    --with-xkeyboard
    --with-xrandr
    --with-xrecord
    --without-macosx-native
    --without-xtrap
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "avahi avahi /usr/$(exhost --target)"
)

